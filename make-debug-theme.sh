#!/bin/bash
DESTDIR=xcursor-debug-theme
rm -rf $DESTDIR
mkdir -p $DESTDIR/{png,cursors}

# $1 = index
# $2 = name
for c in $(< cursor-index); do
	set -- ${c/:/ }
	: ${1:?1st argument missing}
	: ${2:?2nd argument missing}
	SOURCEPNG="$DESTDIR/png/$2.png"
	DESTCURSOR="$DESTDIR/cursors/$2"
	if [ -e $DESTCURSOR -o -e $SOURCEPNG ]; then
		echo "skipping: $DESTCURSOR"
		continue
	fi
	convert \
		-background none -fill 'red'        \
		-draw 'point 0,0' -draw 'point 1,1' \
		-draw 'point 1,0' -draw 'point 0,1' \
		-draw 'point 2,0' -draw 'point 0,2' \
		-pointsize 13 label:"$1: $2"        \
		$SOURCEPNG || break
	echo "24 0 0 $SOURCEPNG"|xcursorgen - $DESTCURSOR || break
	echo "$DESTCURSOR: done"
done
