A stripped-down debug theme, derived from ultr's [dummy X11 cursors](https://www.opendesktop.org/p/999853/).

Dependencies: ImageMagick
